#include "xil_io.h"

#define SW_ADDR  0x40000008
#define LED_ADDR 0x40000000

void main()
{
	int a = 0, out = 0;
	while(1){
	a = Xil_In32(SW_ADDR);
	switch (a & 0x0F) {
	case 1:
		// 01111111_01100000
		out = 0x7f60;
		break;
	case 2:
		// 01111111_11001100
		out = 0x7fcc;
		break;
	case 3:
		// 01111111_11110100
		out = 0x7ff4;
		break;
	case 4:
		// 01111111_01100110
		out = 0x7f66;
		break;
	case 5:
		// 01111111_10100110
		out = 0x7fa6;
		break;
	case 6:
		// 01111111_10100110
		out = 0x7fa6;
		break;
	case 7:
		// 01111111_10100110
		out = 0x7fa6;
		break;
	default:
		// 01111111_10100110
		out = 0x7fa6;
		break;
	}
	Xil_Out32(LED_ADDR, out);
	// 01111111_00000000
	}

}
