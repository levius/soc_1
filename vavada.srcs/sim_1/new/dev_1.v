`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.09.2021 14:45:18
// Design Name: 
// Module Name: dev_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dev_1;

reg clk, reset;
reg [15:0] sw;
wire [15:0] led;

 design_1_wrapper test
   (.clk_100MHz(clk),
    .gpio_rtl_0_tri_o(led),
    .gpio_rtl_1_tri_i(sw),
    .reset_rtl_0(reset));
    
    initial begin
        reset = 0;
        clk = 0;
        sw = 5;
        #100
        reset = 1;
        #100
        reset = 0;
    end
    
    always #20 clk = ~clk;
    
endmodule
